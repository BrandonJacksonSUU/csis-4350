import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { EducationComponent } from './education/education.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { ChatComponent } from './chat/chat.component';
import { ResumeComponent } from './resume/resume.component';
import { WeatherComponent } from './weather/weather.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'education', component: EducationComponent },
  { path: 'technologies', component: TechnologiesComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'weather', component: WeatherComponent},
  { path: 'resume', component: ResumeComponent},

  // otherwise redirect to home
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
