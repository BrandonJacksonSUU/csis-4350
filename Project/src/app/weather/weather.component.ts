import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})

export class WeatherComponent implements OnInit {

  constructor() {
   }

   //Once we open the page we want to populate the webpage
   ngOnInit() {
    const button = document.getElementById("button") as HTMLButtonElement;
    button.onclick = () => this.search();
    this.populateData();
  }

   public async populateData() {
    let apiKey = '169ed05fae1c5f5638830c71394b6dbb';
    let city = 'cedar city';
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`;

    fetch(url).then(res => {
      return res.json();
    }).then(function(res) {

      //If you add <tr>, it will auto add </tr>

      //https://openweathermap.org/weather-conditions

      document.getElementById("table-body").innerHTML = "<tr><td>City: "+ res.name +"</td><td>Country: "+ res.sys.country +"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan = '3'>Date: "+ new Date().toDateString() +"</td></tr>";

      document.getElementById("table-body").innerHTML += "<tr><td>Longitude: "+ res.coord.lon +"</td><td>Latitude: "+ res.coord.lat +"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan = '3'>Current Temp: "+ ((res.main.temp-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp-273.15).toFixed(2) +"°C</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td>Min: "+ ((res.main.temp_min-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp_min-273.15).toFixed(2) +"°C</td><td>Max: "+ ((res.main.temp_max-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp_max-273.15).toFixed(2) +"°C</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan='3'>Weather Description: "+ res.weather[0].description+"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan='3'><img src='/assets/"+res.weather[0].icon+".png' alt = 'Weather Logo'></td></tr>";

      //console.log(document.getElementById("table-body").innerHTML);
    });
  }


  //search for weather for any city
  public async search(){
    console.log((<HTMLInputElement>document.getElementById("search")).value);
    let apiKey = '169ed05fae1c5f5638830c71394b6dbb';
    let city = (<HTMLInputElement>document.getElementById("search")).value;
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`;

    fetch(url).then(res => {
      return res.json();
    }).then(function(res) {

      //If you add <tr>, it will auto add </tr>

      //https://openweathermap.org/weather-conditions

      document.getElementById("table-body").innerHTML = "<tr><td>City: "+ res.name +"</td><td>Country: "+ res.sys.country +"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan = '3'>Date: "+ new Date().toDateString() +"</td></tr>";

      document.getElementById("table-body").innerHTML += "<tr><td>Longitude: "+ res.coord.lon +"</td><td>Latitude: "+ res.coord.lat +"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan = '3'>Current Temp: "+ ((res.main.temp-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp-273.15).toFixed(2) +"°C</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td>Min: "+ ((res.main.temp_min-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp_min-273.15).toFixed(2) +"°C</td><td>Max: "+ ((res.main.temp_max-273.15)*9/5+32).toFixed(2) +"°F / "+ (res.main.temp_max-273.15).toFixed(2) +"°C</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan='3'>Weather Description: "+ res.weather[0].description+"</td></tr>";
      document.getElementById("table-body").innerHTML += "<tr><td colspan='3'><img src='/assets/"+res.weather[0].icon+".png' alt = 'Weather Logo'></td></tr>";

      //console.log(document.getElementById("table-body").innerHTML);
    });
  }

}

/*
body: {
"coord":{"lon":-113.06,"lat":37.68},
"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],
"base":"stations",
"main":{"temp":274.27,"pressure":1022,"humid":"cleity":93,"temp_min":273.71,"temp_max":275.15},
"visibility":16093,
"wind":{"speed":1.32,"deg":3,"tem59},
"clouds":{"all":1},
"dt":1575596409,
"sys":{"type":1,"id":3584,
"country":"US",
"sunrise":1:{"all575556342,"sunset":1575591218},
"timezone":-25200,
"id":5536630,
"name":"Cedar City",
"cod":200:15755}


*/